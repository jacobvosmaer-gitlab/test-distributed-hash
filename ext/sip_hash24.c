#include "extconf.h"
#include "siphash.h"
#include <ruby.h>

static VALUE digest(VALUE self, VALUE data, VALUE seed) {
  uint8_t out[8];

  Check_Type(data, T_STRING);
  Check_Type(seed, T_STRING);
  if (RSTRING_LEN(seed) != 16)
    rb_raise(rb_eArgError, "key must be 16 bytes");

  siphash(StringValuePtr(data), RSTRING_LEN(data), StringValuePtr(seed), out,
          sizeof(out));

  return ULL2NUM((uint64_t)out[0] << (0 * 8) | (uint64_t)out[1] << (1 * 8) |
                 (uint64_t)out[2] << (2 * 8) | (uint64_t)out[3] << (3 * 8) |
                 (uint64_t)out[4] << (4 * 8) | (uint64_t)out[5] << (5 * 8) |
                 (uint64_t)out[6] << (6 * 8) | (uint64_t)out[7] << (7 * 8));
}

void Init_sip_hash24(void) {
  VALUE rb_mSipHash24 = rb_define_module("SipHash24");
  rb_define_module_function(rb_mSipHash24, "digest", digest, 2);
}
