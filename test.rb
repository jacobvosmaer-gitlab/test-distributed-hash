require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'redis', '~> 4.0'
end

require 'securerandom'
require 'redis/hash_ring'
require 'benchmark'

$:.unshift(File.expand_path('../ext', __FILE__))
require 'sip_hash24'

Node = Struct.new(:id, :size)

NUM_NODES = 8
KEYS = Array.new(1000000) { SecureRandom.uuid }
SEED = '0123456789abcdef'

def test_hashring(hr, keys)
  NUM_NODES.times { |i| hr.add_node(Node.new("redis://gprd-redis-cache-#{i}", 0)) }
  keys.each do |key|
    hr.get_node(key).size += 1
  end

  puts
  sizes = hr.nodes.map(&:size)
  printf "%d nodes, %d keys. Max overfill: %.2g%%\n", hr.nodes.size, keys.size, 100*(sizes.max.to_f / (keys.size / hr.nodes.size) - 1)
end

class Rendezvous
  attr_reader :nodes

  def initialize(nodes=[])
    @nodes = []
    nodes.each { |n| add_node(n) }
  end

  def add_node(node)
    @nodes << node
    @nodes.sort_by!(&:id)
  end

  def get_node(key)
    max = nil
    pick = nil

    @nodes.each do |n|
      data = "#{n.id}\x00#{key}"
      hash = SipHash24.digest(data, SEED)
      if max.nil? || hash > max
        max = hash
        pick = n
      end
    end

    pick
  end
end

Benchmark.benchmark do |x|
  {
    'default Redis::Hashring' => Redis::HashRing.new,
    'Redis::Hashring 1024 points' => Redis::HashRing.new([], 1024),
    'Redis::Hashring 2048 points' => Redis::HashRing.new([], 2048),
    'Redis::Hashring 4096 points' => Redis::HashRing.new([], 4096),
    'Redis::Hashring 8192 points' => Redis::HashRing.new([], 8192),
    'Rendezvous' => Rendezvous.new
  }.each do |description, hashring|
    x.report(description) { test_hashring(hashring, KEYS) }
  end
end
